<?php

namespace App\Http\Controllers;

use App\Escuelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EscuelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['escuelas']=Escuelas::paginate(3);
        return view('escuelas.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('escuelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'Nombre' => 'required|string|max:100',
            'Direccion' => 'required|string|max:100',
            'Logotipo' => 'dimensions:min_width=200,min_height=200,size:max:2048'
        ];
        $mensaje=["required" => ':attribute es requerido'];
        $this->validate($request,$campos,$mensaje);
        //
        $escuelasData=request()->except('_token','_method');
        
        if($request->hasFile('Logotipo')){

            $escuelasData['Logotipo']=$request->file('Logotipo')->store('uploads','public');

        }
        Escuelas::insert($escuelasData);
        //return response()->json($escuelasData);
        return redirect('escuelas')->with('mensaje','Escuela creada');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function show(Escuelas $escuelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $escuela = Escuelas::findOrFail($id);
        return view('escuelas.edit', compact('escuela'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'Nombre' => 'required|string|max:100',
            'Direccion' => 'required|string|max:100',
            'Logotipo' => 'dimensions:min_width=200,min_height=200,size:max:2048'
        ];



        $mensaje=["required" => ':attribute es requerido'];

        $this->validate($request,$campos,$mensaje);

        $escuelasData=request()->except(['_token','_method']);
        if($request->hasFile('Logotipo')){
            $escuela = Escuelas::findOrFail($id);
            Storage::delete(['public/'.$escuela->Logotipo]);
            $escuelasData['Logotipo']=$request->file('Logotipo')->store('uploads','public');

        }

        Escuelas::where('id','=',$id)->update($escuelasData);

        $escuela = Escuelas::findOrFail($id);
        return redirect('escuelas')->with('mensaje','Escuela editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $escuela = Escuelas::findOrFail($id);
        if(Storage::delete(['public/'.$escuela->Logotipo])){
            Escuelas::destroy($id);
        }

        Escuelas::destroy($id);
        return redirect('escuelas')->with('mensaje','Escuela borrada');

    }
}
