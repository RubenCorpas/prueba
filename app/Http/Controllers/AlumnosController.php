<?php

namespace App\Http\Controllers;

use App\Alumnos;
use App\Escuelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use illuminate\Support\Facades\DB;

class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $datos['alumnos']=Alumnos::paginate(5);
        

        return view('alumnos.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Escuela = escuelas::all();
        return view('alumnos.create',compact('Escuela'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'Nombre' => 'required|string|max:100',
            'Apellidos' => 'required|string|max:100',
            'Fecha_nacimiento' => 'required|date|max:100'
            
        ];
        $mensaje=["required" => ':attribute es requerido'];
        $this->validate($request,$campos,$mensaje);
        //
        //$alumnosData=request()->all();
        $alumnosData=request()->except(['_token','_method']);
        
        Alumnos::insert($alumnosData);

        //return response()->json($alumnosData);
        return redirect('alumnos')->with('mensaje','Alumno creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function show(Alumnos $alumnos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $alumno = Alumnos::findOrFail($id);
        $Escuela = escuelas::all();
        return view('alumnos.edit', compact('alumno','Escuela'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[
            'Nombre' => 'required|string|max:100',
            'Apellidos' => 'required|string|max:100',
            'Fecha_nacimiento' => 'required|date|max:100'
            
        ];
        $mensaje=["required" => ':attribute es requerido'];
        $this->validate($request,$campos,$mensaje);

        
        //
        
        $alumnosData=request()->except(['_token','_method']);
        

        Alumnos::where('id','=',$id)->update($alumnosData);
        //$alumno = Alumnos::findOrFail($id);
        //return view('alumnos.edit', compact('alumno'));
        return redirect('alumnos')->with('mensaje','Alumno editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Alumnos::destroy($id);
        return redirect('alumnos')->with('mensaje','Alumno borrado');
    }
    
}
