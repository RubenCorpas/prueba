<div class="form-group">
        <label class="custom-control-inline" for ="Nombre">{{'Nombre'}}</label>
        <input class ="form-control {{$errors->has('Nombre') ? 'is-invalid':''}}"type="text" name="Nombre" id="Nombre"
        value="{{ isset($alumno->Nombre)?$alumno->Nombre:old('Nombre')}}">
         {!! $errors->first('Nombre','<div class="invalid-feedback">:message</div>')!!}
    </div>
     
    <div class="form-group">
        <label class="custom-control-inline" for ="Apellidos">{{'Apellidos'}}</label>
        <input class="form-control" type="text" name="Apellidos" id="Apellidos" value="{{ isset($alumno->Apellidos) ? $alumno->Apellidos:''}}">
    </div>
    
    <div class="form-group">
        <label class="custom-control-inline" for ="Fecha_nacimiento">{{'Fecha_nacimiento'}}</label>
        <input class ="form-control" type="text" name="Fecha_nacimiento" id="Fecha_nacimiento" value="{{ isset($alumno->Fecha_nacimiento) ? $alumno->Fecha_nacimiento:''}}">
    </div>
    
    <div class="form-group">
        <label class="custom-control-inline" class="form-group" for ="Ciudad">{{'Ciudad'}}</label>
        <input class ="form-control" type="text" name="Ciudad" id="Ciudad" value="{{ isset($alumno->Ciudad) ? $alumno->Ciudad:''}}">

    </div>

    <div class="form-group">
        <label for ="">{{'Escuela'}}</label>
        <select name="Escuela" id="Escuela" class="form-control">
            @foreach ($Escuela as $listaescuela)
            <option value="{{ $listaescuela['id']}}" >{{$listaescuela['Nombre']}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <input class="btn btn-success" type="submit" value="{{$Modo == 'Crear' ? 'Insertar':'Editar'}}"> 
        <a class="btn btn-primary" href="{{url('alumnos')}}">Volver</a>
    </div>