@extends('layouts.app')

@section('content')


<div class="container">
    
@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert">{{Session::get('mensaje')}}</div>
@endif


<a href="{{url('alumnos/create')}}" class="btn btn-success">Crear Alumnos</a>
<br/>
<br/>
<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nombre y apellidos</th>
            <th>Fecha de nacimiento</th>
            <th>Ciudad</th>
            <th>Escuela</th>
            <th>Opciones<th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($alumnos as $alumno)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$alumno->Nombre}} {{$alumno->Apellidos}}</td>
            <td>{{$alumno->Fecha_nacimiento}}</td>
            <td>{{$alumno->Ciudad}}</td>
            <td>{{$alumno->Escuela}}</td>

            <td>
                <a class="btn btn-warning" href="{{ url('/alumnos/'.$alumno->id.'/edit')}}">Editar</a>
                <form style="display:inline" method="post" action="{{ url('/alumnos/'.$alumno->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
                </form> 
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$alumnos->links()}}
</div>
@endsection