<div class="form-group">

    <label class="custom-control-inline" for ="Nombre">{{'Nombre'}}</label>
    <input class ="form-control {{$errors->has('Nombre') ? 'is-invalid':''}}" type="text" name="Nombre" id="Nombre" value="{{ isset($escuela->Nombre)?$escuela->Nombre:old('Nombre')}}">
    {!! $errors->first('Nombre','<div class="invalid-feedback">:message</div>')!!}
</div>

<div>

    <label  class="custom-control-inline" for ="Direccion">{{'Direccion'}}</label>
    <input  class ="form-control {{$errors->has('Direccion') ? 'is-invalid':''}}" type="text" name="Direccion" id="Direccion" value="{{ isset($escuela->Direccion)?$escuela->Direccion:old('Direccion')}}">
    {!! $errors->first('Direccion','<div class="invalid-feedback">:message</div>')!!}
</div>


<div class="form-group">
    
    <label  class="custom-control-inline"for ="Logotipo">{{'Logotipo'}}</label>
    <input  class ="form-control" type="file" name="Logotipo" id="Logotipo" value="">
    @if(isset($escuela->Logotipo))
    <br/>
    <img src="{{ asset('storage'). '/' .$escuela->Logotipo}}" class="img-thumbnail img-fluid" alt="" width="100">  
    <br/>
    @endif
</div>
<div class="form-group">
    <label  class="custom-control-inline"for ="Email">{{'Email'}}</label>
    <input  class ="form-control" type="email" name="Email" id="Email" value="{{ isset($escuela->Email)?$escuela->Email:old('Email')}}">
</div>
<div class="form-group">
    <label  class="custom-control-inline" for ="Telefono">{{'Telefono'}}</label>
    <input  class ="form-control"type="text" name="Telefono" id="Telefono" value="{{ isset($escuela->Telefono)?$escuela->Telefono:old('Telefono')}}">
</div>
   
<div class="form-group">

    <label  class="custom-control-inline" for ="Pagina_web">{{'Pagina_web'}}</label>
    <input  class ="form-control" type="text" name="Pagina web" id="Pagina_web" value="{{ isset($escuela->Pagina_web)?$escuela->Pagina_web:old('Pagina_web')}}">
</div>

<div class="form-group">
    <input class="btn btn-success" type="submit" value="{{$Modo == 'Crear' ? 'Insertar':'Editar'}}"> 
    <a class="btn btn-primary" href="{{url('escuelas')}}">Volver</a>
</div>
