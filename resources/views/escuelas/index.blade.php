@extends('layouts.app')

@section('content')


<div class="container">
    
@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert">{{Session::get('mensaje')}}</div>
@endif

<a href="{{url('escuelas/create')}}" class="btn btn-success">Crear Escuelas</a>
<br/>
<br/>
<table class="table table-light table-hover">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Logotipo</th>
            <th>Nombre</th>
            <th>Dirección</th>
            <th>Email</th>
            <th>Telefono</th>
            <th>Pagina web</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($escuelas as $escuela)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td><img src="{{ asset('storage'). '/' .$escuela->Logotipo}}" class="img-thumbnail img-fluid" alt=""  width="70">       
            </td>
            <td>{{$escuela->Nombre}}</td>
            <td>{{$escuela->Direccion}}</td>
            <td>{{$escuela->Email}}</td>
            <td>{{$escuela->Telefono}}</td>
            <td>{{$escuela->Pagina_web}}</td>
            <td>
            <a class="btn btn-warning" href="{{ url('/escuelas/'.$escuela->id.'/edit')}}">Editar</a>
            <form method="post" action="{{ url('/escuelas/'.$escuela->id)}}" style="display:inline">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
            </form> 
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$escuelas->links()}}
</div>
@endsection